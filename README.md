# Resizable plugin ver. 1.0.0

## Getting started

To download plugin files visit [Documentation site](https://resizable-plugin.firebaseapp.com)

## Installation

To install resizable plugin include CSS file to the **Head** section

```
<link rel="stylesheet" href="resizable.css">
```

You'll need insert javascript before closing **Body** tag

```
<script src="resizable.js"></script>
```

## Basic usage

To show resizable element put the following code snippets

```
<div id="example"></div>
```

```
const resize = new Resizable({
	el:'#example',
	rows:4,
	columns:4,
	cellWidth:75,
	cellHeight:60
});
```


## Options

```
const resize = new Resizable({
	el:'string',
	rows:'number',
	columns:'number',
	cellWidth:'number',
	cellHeight:'number',
	style:'object',
	onChange:'function'
});
```

## Required properties

The following properties are needed for the plugin to function properly



## Styling

You can change draggable element properties very simple using css selector

```
<div id="resize-styled"></div>
```

```
#resize-styled{
	background-color:#5431d6;
}
```

To modify grid container you have to use **style** property

```
const resizeStyled = new Resizable({
	el:'#resize-styled',
	rows:4,
	columns:4,
	cellWidth:75,
	cellHeight:60,
	style:{
		gridColor:'#7b69bb',
		backgroundColor:'#213242',
		margin:'0 0 20px 0'
	}
});
```

## onChange method

Method **onChange** activates every time that **element** size is changing

```
const change = new Resizable({
	el:'#on-change',
	rows:3,
	columns:3,
	cellWidth:75,
	cellHeight:60,
	onChange(){
		let rows = this.getRow;
		let columns = this.getColumn;
		demo.innerHTML = `Rows: ${rows} <br> Columns: ${columns}`;
	}
});
```

Demo - [onChange demo](https://resizable-plugin.firebaseapp.com/#methods)

## License

**This project is licensed under the MIT License - see the LICENSE file for details**


