const openMenu = document.querySelector('.menu-open');
const closeMenu = document.querySelector('.menu-close');
const navigation = document.querySelector('.page-navigation');

openMenu.addEventListener('click',()=>{
    navigation.classList.add('active');
});

closeMenu.addEventListener('click',()=>{
    navigation.classList.remove('active');
})