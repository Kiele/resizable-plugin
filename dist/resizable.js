class Resizable {
	constructor({el,rows,columns,cellWidth,cellHeight,onChange,style}){
		el = document.querySelectorAll(el);
		el.forEach(item => {
			this.chw = cellWidth*columns;
			this.chh = cellHeight*rows;
			this.cellWidth = cellWidth;
			this.cellHeight = cellHeight;
			this.rows = rows;
			this.columns = columns;
			const grid = document.createRange().createContextualFragment(`
			<div id="resize-container">
				<div style="position:relative" class="resize-element" id="${item.getAttribute('id')}">
					<div id="resize-handle"></div>
				</div>
			</div>`);
			const gridContainer = grid.querySelector('#resize-container');
			const handle = grid.querySelector('#resize-handle');
			this.el = grid.querySelector('.resize-element');
			gridContainer.style.width = this.chw;
			gridContainer.style.cellHeight = this.chh;
			gridContainer.style.gridTemplate = `repeat(${rows},${cellHeight}px)/repeat(${columns},${cellWidth}px)`;
			if(style){
				const {
					gridColor,
					backgroundColor,
					margin
				} = style;
				gridContainer.style.backgroundColor = backgroundColor;
				gridContainer.style.margin = margin;
			}
			for(let i=0;i<=rows;i++){
				const line = document.createElement('div');
				line.setAttribute('id','horizontal-line');
				line.style.top = `${cellHeight*i}px`;
				if(style && style.gridColor){
					line.style.backgroundColor = style.gridColor;
				}
				gridContainer.append(line);
			}
			for(let i=0;i<=columns;i++){
				const line = document.createElement('div');
				line.setAttribute('id','vertical-line');
				line.style.left = `${cellWidth*i}px`;
				if(style && style.gridColor){
					line.style.backgroundColor = style.gridColor;
				}
				gridContainer.append(line);
			}
			item.replaceWith(grid);
			if(typeof onChange === 'function'){
				this.onChange = onChange;
			}
			handle.addEventListener('mousedown',this.start);
			handle.addEventListener('mousemove',this.resize);
			document.addEventListener('mouseup',this.end);
		});
	}
	start = e =>{
		this.resizeElement = this.el;
		this.border = parseInt(getComputedStyle(this.resizeElement).getPropertyValue('border-left-width'))*2;
		this.elOffset = this.resizeElement.getBoundingClientRect();
		this.elWidth = this.elOffset.width+this.border;
		this.elHeight = this.elOffset.height+this.border;
		this.elMouseX = e.clientX-this.elOffset.left;
		this.elMouseY = e.clientY-this.elOffset.top;
	}
	resize = e =>{
		if(!this.resizeElement)
			return;
		this.resizeElement.style.width = `${this.elWidth+e.clientX-this.elMouseX-this.elOffset.left}px`;
		this.resizeElement.style.height = `${this.elHeight+e.clientY-this.elMouseY-this.elOffset.top}px`;
		if(parseInt(this.resizeElement.style.width)+this.border>this.chw)
			this.resizeElement.style.width = `${this.chw}px`;
		if(parseInt(this.resizeElement.style.height)+this.border>this.chh)
			this.resizeElement.style.height = `${this.chh}px`;
		if(parseInt(this.resizeElement.style.width)+this.border<this.cellWidth)
			this.resizeElement.style.width = `${this.cellWidth}px`;
		if(parseInt(this.resizeElement.style.height)+this.border<this.cellHeight)
			this.resizeElement.style.height = `${this.cellHeight}px`;
	}
	end = () =>{
		if(!this.resizeElement)
			return;
		const off = this.resizeElement.getBoundingClientRect();
		this.getColumn = Math.round((off.width+this.border-this.cellWidth)/this.cellWidth)+1;
		this.getRow = Math.round((off.height+this.border-this.cellHeight)/this.cellHeight)+1;
		this.resizeElement.style.width = `${this.getColumn*this.cellWidth}px`;
		this.resizeElement.style.height = `${this.getRow*this.cellHeight}px`;
		this.resizeElement = null;
		this.onChange ? this.onChange() : null
	}
}
const resize = new Resizable({
	el:'#example',
	rows:4,
	columns:4,
	cellWidth:75,
	cellHeight:60
});

const demo = document.querySelector('.change-demo-box');

const change = new Resizable({
	el:'#on-change',
	rows:3,
	columns:3,
	cellWidth:75,
	cellHeight:60,
	style:{
		margin:'20px 0 0 0'
	},
	onChange(){
		let rows = this.getRow;
		let columns = this.getColumn;
		demo.innerHTML = `Rows: ${rows} <br> Columns: ${columns}`;
	}
});

const resizeStyled = new Resizable({
	el:'#resize-styled',
	rows:4,
	columns:4,
	cellWidth:75,
	cellHeight:60,
	style:{
		gridColor:'#7b69bb',
		backgroundColor:'#213242',
		margin:'0 0 20px 0'
	}
});